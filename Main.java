package teste;

public class Main {
    
        public static void main(String[] args) {
            Pilha pilha = new Pilha();
            
            //operacoes de push
            pilha.push(10);
            pilha.push(5);
            pilha.push(3);
            pilha.push(9);
            pilha.push(1);
            pilha.push(0);
            pilha.push(2);
            pilha.push(7);
            pilha.push(4);   
            
            //operacoes de pop e min
            pilha.pop();
            pilha.pop();
            System.out.println("Min(): "+pilha.min());
            pilha.pop();
            pilha.pop();
            System.out.println("Min(): "+pilha.min());
            pilha.pop();
            System.out.println("Min(): "+pilha.min());
            pilha.pop();
            pilha.pop();
            System.out.println("Min(): "+pilha.min());
            pilha.pop();
            pilha.pop();
        }
        
        
}