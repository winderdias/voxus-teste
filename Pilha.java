package teste;

import java.util.ArrayList;
import java.util.EmptyStackException;

public class Pilha {
	
        //Pilha de inteiros
	private ArrayList<Integer> pilha;
        
        //Pilha que contem os menores inteiros no topo
        private ArrayList<Integer> pilhaMenores;
        
        public Pilha(){
            this.pilha  = new ArrayList<Integer>();
            this.pilhaMenores = new ArrayList <Integer>();
        }
        
        //metodo responsavel pela adicao de inteiros no topo da pilha
        public void push (int inteiro){
            
            //adiciona inteiro ao topo da pilha
            this.pilha.add(inteiro);
            
            //se a pilha estiver vazia
            if(this.pilhaMenores.size() == 0)
                this.pilhaMenores.add(inteiro);
            
            else{
                //recebe o indice do topo da pilha de menores
                int topo = this.pilhaMenores.size()-1;
                //caso contrario, compara com o topo da pilha de menores
                if(this.pilhaMenores.get(topo) > inteiro){
                    //atualiza o topo da pilha com o menor
                    this.pilhaMenores.add(inteiro);
                }
            }
        }
        
        //metodo responsavel pela remocao de inteiros no topo da pilha
        public void pop () {
            
            //recebe o indice do topo da pilha
            int topo = this.pilha.size()-1;
            
            //recebe o indice do topo da pilha de menores
            int topoMenores = this.pilhaMenores.size()-1;
            
            //se a pilha nao estiver vazia
            if(this.pilhaMenores.size()>0){
                
                //se o inteiro a ser removido for o menor
                if(this.pilhaMenores.get(topoMenores) == this.pilha.get(topo))
                    this.pilhaMenores.remove(topoMenores);
                
                //remove o topo da pilha
                this.pilha.remove(topo);
            }
            
            
        }
        //metodo retorna menor inteiro da pilha
        public int min () throws EmptyStackException{
            
            //se a pilha estiver vazia gera excecao
            if(this.pilhaMenores.size()==0){
                System.out.println("Nao foi possivel retornar o minimo, pilha vazia !!!");
                throw new EmptyStackException();
            }
            
            //recebe o topo da pilha dos menores
            int topo = this.pilhaMenores.size()-1;
            
            //retorna o menor da pilha
            return this.pilhaMenores.get(topo);
        }
        
}
