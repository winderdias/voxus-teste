# README #
# Teste das Filas #
Para a implementação deste teste, duas listas de inteiros foram utilizadas. A primeira lista é a pilha onde as operações de pop e push são executadas. A segunda lista é outra pilha, cujo objetivo é armazenar no topo somente os menores valores já inseridos. A segunda pilha é importante para manter o valor mínimo sempre atualizado, já que as operações de pop podem excluir um valor que antes era considerado mínimo. Após a exclusão, um novo valor deve ser adotado como minímo. Esse valor está sempre no topo da pilha auxiliar que armazena os menores.

Todos os métodos foram implementados em complexidade de tempo constante O(1). 

O método de push() apenas adiciona ao topo das pilhas um novo inteiro, essa ação tem complexidade constante.

O método de pop() também remove o inteiro que está no topo da pilha, caso esse inteiro for o menor, remove também da pilha que contém os menores, essas ações também são O(1).

O método min() apenas retorna o topo da pilha que armazena os menores inteiros, essa ação também tem complexidade constante.

O método pilha.size() utilizado para verificar o tamanho da pilha tem complexidade de tempo constante, como pode ser visto em:
http://www.docjar.com/html/api/java/util/LinkedList.java.html